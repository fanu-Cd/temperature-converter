//A Loader Function called when the 'Go' Button Is Clicked:-

function load(){
    if(sel1.value===sel2.value){
        ;
    }
    else{
        document.getElementById('convBtn').disabled=false; 
        document.getElementById('inpt').style.visibility='visible';
        document.getElementById('res').innerHTML='0';       
    }
}

//A Function that checks the scale entries and leads to the calculator Functions

 function calc(v){
    if(sel1.value==='c'&&sel2.value==='f')
      cTof(v);
    else if(sel1.value==='c'&&sel2.value==='k')
      cTok(v);
    else if(sel1.value==='f'&&sel2.value==='c')
      fToc(v);
    else if(sel1.value==='f'&&sel2.value==='k')
      fTok(v);
    else if(sel1.value==='k'&&sel2.value==='c')
      kToc(v);
    else if(sel1.value==='k'&&sel2.value==='f') 
      kTof(v);
    else
    ;
}

//Functions that do the calclations based on the user's input

 function cTof(c){
    let f=(9/5)*c+32;
    document.getElementById('res').innerHTML=f.toFixed(4);
    document.getElementById('convBtn').disabled='true';
 }
 function cTok(c){
    let k=c+273.15;
    document.getElementById('res').innerHTML=k.toFixed(4);
    document.getElementById('convBtn').disabled='true';
 }
 function fToc(f){
    let c=(5/9)*f-32;
    document.getElementById('res').innerHTML=c.toFixed(4);
    document.getElementById('convBtn').disabled='true';
 }
 function fTok(f){
    let k=(5/9)*f+(459.67);
    document.getElementById('res').innerHTML=k.toFixed(4);
    document.getElementById('convBtn').disabled='true';
 }
 function kToc(k){
    let c=k-273.15;
    document.getElementById('res').innerHTML=c.toFixed(4);
    document.getElementById('convBtn').disabled='true';
 }
 function kTof(k){
    let f=((9/5)*k)-(827.406);
    document.getElementById('res').innerHTML=f.toFixed(4);
    document.getElementById('convBtn').disabled='true';
 }



 //JS CODE BY FANUEL AMARE